const Utils = require('./utils');
const Items = require('./objects/items.json');
const Dungeons = require('./objects/dungeons.json');

const RAID_WEAK_MONSTER = 0;
const RAID_NORMAL_MONSTER = 1;
const RAID_STEP_KILL = 0;
const RAID_STEP_ATTACK_BOSS = 1;
const RAID_STEP_LORE_INTRO = 10;
const RAID_STEP_BOSS_INTRO = 11;
const RAID_STEP_FAIL = 12;
const RAID_STEP_WIN = 13;
const RAID_LAST_STEP = 20;

const ITEM_ATTACK = 0;
const ITEM_MAGIC_ATTACK = 1;
const ITEM_DEFEND = 10;
const ITEM_LUCK = 11;

class Raid {
	constructor(channel) {
		this.channel = channel;
		this.playerCount = 0;
		this.users = {};
		this.dungeon = Utils.randItem(Dungeons);

		this.locked = false;
	}

	join(userID, user) {
		this.users[userID] = user;
		this.playerCount++;
	}

	destroy() {
		this.users = {};
		this.playerCount = 0;
		this.channel = null;
		this.locked = true;
	}

	randomMonster(type) {
		if (type == RAID_WEAK_MONSTER) {
			return Utils.randItem(this.dungeon.weak_monsters);
		} else if (type === RAID_NORMAL_MONSTER) {
			return Utils.randItem(this.dungeon.normal_monsters);
		}
	}

	randomUser() {
		var users = Object.entries(this.users),
			rand = Utils.randItem(users);
		return { userID: rand[0], user: rand[1] };
	}

	realize() {
		var steps = [],
			monsterAttacks = [],
			dungeon = this.dungeon,
			rand = Utils.rand;

		this.locked = true;

		// create [10,20] weak monsters [3,5] times
		for (let i = 0; i < rand(3, 5); i++) {
			let { userID, user } = this.randomUser(),
				equippedName = user.equipped,
				equippedItem = Items[equippedName];

			let monsterCountBuff = 0,
				goldBuff = 0;

			if (equippedName === undefined || Items.hasOwnProperty(equippedName) !== true) {
				// TODO remove this?
				monsterCountBuff = 0;
				goldBuff = 0;
			} else if (equippedItem.type === ITEM_ATTACK) {
				monsterCountBuff = equippedItem.attack;
			} else if (equippedItem.type === ITEM_LUCK) {
				goldBuff = equippedItem.luck;
			}

			let monsterCount = rand(10 + monsterCountBuff, 20 + monsterCountBuff),
				monsterName = this.randomMonster(RAID_WEAK_MONSTER),
				gold = rand(1 + goldBuff, 2 + goldBuff) * 10 * monsterCount,
				xp = 2 * monsterCount;

			monsterAttacks.push({
				type: RAID_STEP_KILL,
				userID,
				user,
				monsterCount,
				monsterName,
				gold,
				xp
			});
		}

		// create [2,10] normal monsters [1,4] times
		for (let i = 0; i < rand(1, 4); i++) {
			let { userID, user } = this.randomUser(),
				equippedName = user.equipped,
				equippedItem = Items[equippedName];

			let monsterCountBuff = 0,
				goldBuff = 0;

			if (equippedName === undefined || Items.hasOwnProperty(equippedName) !== true) {
				// TODO remove this?
				monsterCountBuff = 0;
				goldBuff = 0;
			} else if (equippedItem.type === ITEM_ATTACK) {
				monsterCountBuff = equippedItem.attack;
			} else if (equippedItem.type === ITEM_LUCK) {
				goldBuff = equippedItem.luck;
			}

			let monsterCount = rand(2 + monsterCountBuff, 10 + monsterCountBuff),
				monsterName = Utils.randItem(dungeon.normal_monsters),
				gold = rand(3 + goldBuff, 4 + goldBuff) * 10 * monsterCount,
				xp = 3 * monsterCount;

			monsterAttacks.push({
				type: RAID_STEP_KILL,
				userID,
				user,
				monsterCount,
				monsterName,
				gold,
				xp
			});
		}

		// mix the weak/normal monsters
		monsterAttacks = Utils.shuffle(monsterAttacks);

		steps.push({ type: RAID_STEP_LORE_INTRO });
		steps = steps.concat(monsterAttacks);
		steps.push({ type: RAID_STEP_BOSS_INTRO });

		// start the boss step
		// damages stores what kind of attacks will be done, hopefully median/average/sum = 1
		var boss = dungeon.boss,
			partsAttacked = rand(0, 2) + boss.parts.length,
			// damages = [ 2, 1, 1, 1, -1, -1, -2 ],
			damages = [ 1, 1, 1, -1, -1, -2 ],
			dealtDamage = 0;

		for (let i = 0; i < partsAttacked; i++) {
			let { userID, user } = this.randomUser(),
				equippedName = user.equipped,
				equippedItem = Items[equippedName];

			let part = Utils.randItem(boss.parts),
				attack = Utils.randItem(damages),
				damageBuff;

			if (equippedName === undefined || Items.hasOwnProperty(equippedName) !== true) {
				// TODO remove this?
				damageBuff = 0;
			} else if (equippedItem.type === ITEM_ATTACK || equippedItem.type === ITEM_MAGIC_ATTACK) {
				damageBuff = Math.floor(equippedItem.attack / 10);
			}

			// 20% chance to add weapon's attack / 10
			// the `attack / 10` is three lines above this
			dealtDamage += attack;
			if (rand(0, 10) < 2)
				dealtDamage += damageBuff;

			steps.push({
				type: RAID_STEP_ATTACK_BOSS,
				attack: attack + damageBuff,
				part,
				userID,
				user
			});
		}

		dealtDamage += rand(-1, 1);

		if (dealtDamage <= 0)
			steps.push({ type: RAID_STEP_FAIL });
		else
			steps.push({ type: RAID_STEP_WIN });

		steps.push({ type: RAID_LAST_STEP });

		return steps;
	}
}

module.exports = Raid;