function rand(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min)
}

function time() {
	return Math.floor(Date.now() / 1000)
}

function pronoun() {
	var possible = ['bro', 'bro', 'bro', 'dude', 'fam', 'm8']
	return randItem(possible)
}

function randomString(length) {
	var result = '',
		chars = 'ybndrfg8ejkmcpqxot1uwisza345h769';
	for (var i = length; i > 0; --i)
		result += randItem(chars)

	return result
}

function randItem(arr) {
	return arr[Math.floor(Math.random() * arr.length)]
}

// https://stackoverflow.com/a/6274398
function shuffle(array) {
	let counter = array.length;

	// While there are elements in the array
	while (counter > 0) {
		// Pick a random index
		let index = Math.floor(Math.random() * counter);

		// Decrease counter by 1
		counter--;

		// And swap the last element with it
		let temp = array[counter];
		array[counter] = array[index];
		array[index] = temp;
	}

	return array;
}

module.exports = {
	rand,
	time,
	pronoun,
	randomString,
	randItem,
	shuffle
}