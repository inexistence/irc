const fs = require('fs')
const readline = require('readline')

const LINE_END = '\r\n';

class Storage {
	constructor(name) {
		this.name = name;
		this.MEMORY = {};
		this.hasConsolidated = false;
		this.loaded = false;

		try {
			// this will throw if the file doesnt exist :^)
			fs.accessSync(this.name, fs.constants.F_OK);
		} catch (ex) {
			// touch the file
			fs.writeFileSync(this.name, '');
		}
	}

	load(callback) {
		this.loaded = true;

		fs.access(this.name, fs.constants.R_OK | fs.constants.W_OK, (err) => {
			// just complain loudly to the user if the file cant be read/written since we already did a check in constructor()
			if(err)
				throw err;

			var readable = fs.createReadStream(this.name),
				stream = readline.createInterface({ input: readable });

			var reconstruction = {};

			stream.on('line', (line) => {
				var obj = JSON.parse(line);
				reconstruction[obj[0]] = obj[1];
			});

			readable.on('end', () => {
				this.MEMORY = reconstruction;
				callback();
			});
		});
	}

	consolidate(callback) {
		if(this.shouldSave === false)
			return;

		var writable = fs.createWriteStream(this.name);
		this.shouldSave = false;

		for(let key in this.MEMORY) {
			var asArray = [ key, this.MEMORY[key] ],
				serialized = JSON.stringify(asArray);

			writable.write(serialized + LINE_END);
		}

		writable.on('fininsh', () => callback());

		writable.end();
	}

	_append(key, document) {
		var contents = [ key, document ],
			toAppend = JSON.stringify(contents) + LINE_END;

		this.shouldSave = true;

		fs.appendFile(this.name, toAppend, (err) => {
			if(err)
				throw err;
		});
	}

	get(key) {
		if(!this.loaded)
			throw new Error('database is not loaded');

		if(typeof key !== 'string')
			return undefined;

		var document;

		if(this.MEMORY.hasOwnProperty(key) === true)
			document = this.MEMORY[key];
		else
			document = undefined;

		return document;
	}

	put(key, document) {
		if(!this.loaded)
			throw new Error('database is not loaded');

		if(typeof key !== 'string')
			throw new Error('use a string as a key you idiot');

		this.MEMORY[key] = document;
		this._append(key, document);
	}

	exists(key) {
		if(!this.loaded)
			throw new Error('database is not loaded');

		if(typeof key !== 'string')
			return false;

		return this.MEMORY.hasOwnProperty(key);
	}
}

module.exports = Storage;