"use strict";

var network,
	settings,
	core,
	modules;

const debug = require('debug')('littleguy');

const Utils = require('./utils');
const User = require('./user');
const Raid = require('./raid');
const Shop = require('./shop');
const ItemRanking = require('./objects/item-ranking.json');
const Storage = require('./storage');

const Settings = new Storage(__dirname + '/settings');
Settings.load(() => {
	debug('settings loaded');
	Settings.consolidate(() => debug('solidified settings'));
});

const Accidents = require('./objects/accidents.json');
const Classes = require('./objects/classes.json');
const Dungeons = require('./objects/dungeons.json');
const Items = require('./objects/items.json');
const Monsters = require('./objects/monsters.json');
const Messages = require('./objects/messages.json');

const ITEM_ATTACK = 0;
const ITEM_MAGIC_ATTACK = 1;
const ITEM_DEFEND = 10;
const ITEM_LUCK = 11;

const CLASS_TYPE_NORMAL = 0;
const CLASS_TYPE_MAGIC = 1;

const ADV_TIMEOUT = 2 * 60;
const ACCIDENT_TIMEOUT = 8 * 60;
const RAID_ADVENTURE_TIMEOUT = 6 * 60; // adventure timeout
const RAID_TIMEOUT = 40; // countdown for the raid queue
const ADV_COOLDOWN = {};

const RAIDS = {};
const RAID_WEAK_MONSTER = 0;
const RAID_NORMAL_MONSTER = 1;
const RAID_STEP_KILL = 0;
const RAID_STEP_ATTACK_BOSS = 1;
const RAID_STEP_LORE_INTRO = 10;
const RAID_STEP_BOSS_INTRO = 11;
const RAID_STEP_FAIL = 12;
const RAID_STEP_WIN = 13;
const RAID_LAST_STEP = 20;

const RAID_BOSS_REWARD_XP = 20;
const RAID_MESSAGE_INTERVAL = 3500;

function setup(bot) {
	({ network, settings, core, modules } = bot);

	if (!core.help)
		throw new Error('requires help core module');

	// name, commands, summary, extra
	core.help.add(
		'class',
		['.class'],
		'lets u view or choose a class for ur little guy.'
	);

	core.help.add(
		'littleguy',
		['.littleguy', '.lilguy', '.lg'],
		'view ur little guys summary, inventory, equip stuff and more. check \x02.littleguy help\x02 for all available sub-commands.'
	);

	core.help.add(
		'adventure',
		['.adventure', '.adv'],
		'go on adventures! with your little guy ofc'
	);

	core.help.add(
		'raid',
		['.raid', '.dungeon', '.partyraid'],
		'start (or join) a raid. its tied to the channel, and \x02spams a lot\x02'
	);

	core.help.add(
		'shop',
		['.shop'],
		'an old man\'s shop'
	);

	return;
}

function handler(irc) {
	irc.on('command', event => {
		if (event.shouldIgnore)
			return;

		var { tag, args, admin, target } = event.command,
			nick = event.from;

		const speak = message => irc.send(target, message);
		const say = message => irc.send(target, nick + ': ' + message);
		const whisper = message => irc.notice(nick, message);

		var userID = nick.toLowerCase();

		if (tag === ':littleguy' || tag === ':lilguy' || tag === ':lg') {
			if (event.command.admin !== true) {
				whisper('ur not an admin');
				return;
			}

			if (args.length < 1 || args[0] === 'help') {
				whisper('syntax: \x02:littleguy\x02 {reload|blacklist}');
			}

			else if (args[0] === 'reload') {
				Settings.load(() => {
					debug('settings reloaded');
					whisper('settings reloaded');
				});
			}

			else if (args[0] === 'blacklist') {
				if (args.length < 2) {
					var blacklist = Settings.get('blacklist');

					if (blacklist === undefined) {
						whisper('there isnt a blacklist');
						return;
					}

					let msg = 'blacklisted chans:';

					for (let chan in blacklist) {
						msg += ' ' + chan + ' (' + blacklist[chan].reason + ')';
					}

					whisper(msg);
				}

				else if (args[1] === 'add') {
					if (args.length < 3) {
						whisper('syntax: \x02:littleguy\x02 blacklist add <channel>');
						return;
					}

					var blacklist = Settings.get('blacklist'),
						target = args[2];

					if (blacklist === undefined) {
						blacklist = {};
						blacklist[target] = {
							reason: '-',
							time: Utils.time()
						}
					}

					Settings.put('blacklist', blacklist);
					whisper('blacklisted: ' + target);
				}

				else if (args[1] === 'del') {
					if (args.length < 3) {
						whisper('syntax: \x02:littleguy\x02 blacklist del <channel>');
						return;
					}

					var blacklist = Settings.get('blacklist'),
						target = args[2];

					if (blacklist === undefined) {
						whisper('the blacklist is empty');
						return;
					}

					if (blacklist.hasOwnProperty(target) !== true) {
						whisper('that chan isnt on the blacklist');
						return;
					}

					delete blacklist[target];
					Settings.put('blacklist', blacklist);
				} else {
					whisper('syntax: \x02:littleguy\x02 blacklist [add|del|help]');
					return;
				}
			}

			else {
				whisper('\x02:littleguy help\x02');
			}
		}

		else if (tag === '.class') {
			if (args.length === 0) {
				if (User.exists(userID)) {
					var user = new User(userID);
					say('ur current class is ' + user.class + '. change it with \x02.class <name>\x02');
				} else {
					say(Utils.pronoun() + ' choose a class with \x02.class <name>\x02 im sending u the list via pm');
				}

				for (let key in Classes) {
					let value = Classes[key];
					if (value.hidden)
						continue;
					whisper(value.tag + ' ' + value.desc);
				}

			} else if (args.length >= 1) {
				var desiredClass = args.join(' ').trim();

				if (Classes.hasOwnProperty(desiredClass) !== true) {
					say(Utils.pronoun() + ' \x02' + desiredClass + '\x02 isnt a valid class');
					return;
				}

				var Class = Classes[desiredClass];

				if (Class.hidden) {
					say('sorry ' + Utils.pronoun() + ' but that class is unavailable for u');
					return;
				}

				if (User.exists(userID)) {
					let user = new User(userID);
					if (user.class === desiredClass) {
						say('ur little guy is already a ' + desiredClass);
						return;
					}

					user.class = desiredClass;
					say('would u look at that! ur little guy is now a ' + desiredClass);
				} else {
					let user = User.create(userID, { nick: nick, class: desiredClass });

					say('gotcha ' + Utils.pronoun() + ', ur little guy is now a ' + desiredClass);
				}

			}
		}

		else if (tag === '.littleguy' || tag === '.lilguy' || tag === '.lg') {
			if (args[0] && (args[0] === 'help' || args[0] === 'h')) {
				say('im sending u the information via pm');
				whisper('send little guys into adventures! commands: https://pastebin.com/raw/jkraXdBm info on using and buying equipment is in the pastebin');
				whisper('most used commands: \x02.adv\x02 go on an adventure!  -  \x02.littleguy\x02 show an overview');
				// whisper('\x02.littleguy\x02              overview');
				// whisper('\x02.littleguy inventory\x02    view the little guys inventory');
				// whisper('\x02.littleguy equip <name>\x02 make your little guy equip something');
				// whisper('\x02.adventure\x02   go on an adventure! 2 min cooldown');
				// whisper('\x02.raid\x02        join a party raid');
				// whisper('\x02.class\x02       selects a new class');
				// whisper('\x02.shop\x02        list what the wise old man is selling');
				return;
			}

			if (!User.exists(userID)) {
				say(Utils.pronoun() + ' u dont have a little guy, do \x02.class\x02');
				return;
			}

			var user = new User(userID);

			if (args.length < 1) {
				let msg = 'ur little guy: ',
					Class = Classes[user.class],
					equipped = user.equipped;

				if (equipped === undefined)
					equipped = 'nothing';

				msg += Class.tag;
				msg += ' | ' + user.gold + ' gold coins';
				msg += ' | ' + user.xp + ' xp';
				msg += ' | ' + equipped + ' equipped';
				msg += ' | for help do \x02.littleguy help\x02';
				say(msg);
			}

			else if (args[0][0] === '@' || args[0][0] === '!') {
				let wantedUserID = args[0].substr(1).toLowerCase();

				if (wantedUserID === userID) {
					say('pls');
					return;
				}

				if (!User.exists(wantedUserID)) {
					say('theres no one by that name');
					return;
				}

				let wantedUser = new User(wantedUserID),
					msg = wantedUser.nick + 's little guy: ',
					Class = Classes[wantedUser.class],
					equipped = wantedUser.equipped;

				if (equipped === undefined)
					equipped = 'nothing';

				msg += Class.tag;
				msg += ' | ' + wantedUser.gold + ' gold coins';
				msg += ' | ' + wantedUser.xp + ' xp';
				msg += ' | ' + equipped + ' equipped';
				say(msg);
			}

			else if (args[0] === 'rank' || args[0] === 'best' || args[0] === 'ranking') {
				var best = User.ranking(),
					list = [];

				if (best.length < 4) {
					say('there are not enough little guys');
					return;
				}

				for (let i = 0; i < best.length; i++) {
					let temp = best[i][0] + ' (',
						points = best[i][1];

					if (points >= 1000000)
						temp += (Math.floor(points / 100000) / 10) + 'm';
					else if (points >= 1000)
						temp += Math.floor(points / 1000) + 'k';
					else
						temp += points;

					temp += ')';
					list.push(temp);
				}

				say('best littleguys: ' + list.join(', '));
			}

			else if (args[0] === 'e' || args[0] === 'equip') {
				var desired = args.splice(1).join(' ').trim();

				if (desired.length < 1) {
					say('u gotta equip something, \x02.lg equip <name>\x02');
					return;
				}

				if (Items.hasOwnProperty(desired) !== true) {
					say(Utils.pronoun() + ' \x02' + desired + '\x02 isnt a thing');
					return;
				}

				if (user.equipped === desired) {
					say('ur little guy already has \x02' + desired + '\x02 equipped');
					return;
				}

				if (user.items.includes(desired) !== true) {
					say(Utils.pronoun() + ' u dont have a \x02' + desired + '\x02');
					return;
				}

				var previouslyEquipped = user.equipped;

				user.equipped = desired;
				if (previouslyEquipped === undefined)
					say('ur little guy equips \x02' + desired + '\x02');
				else
					say('ur little guy stores \x02' + previouslyEquipped + '\x02 and equips \x02' + desired + '\x02');
			}

			else if (args[0] === 'i' || args[0] === 'inv' || args[0] === 'inventory' || args[0] === 'bag') {
				let msg = 'your little guys inventory: ',
					items = user.items;

				msg += user.gold + ' gold coins';

				for (let i = 0; i < items.length; i++) {
					var itemName = items[i],
						item = Items[itemName];

					if (Items.hasOwnProperty(itemName) !== true)
						continue;

					if (item.sprite)
						msg += ' | ' + item.sprite + ' ' + item.name;
					else
						msg += ' | ' + item.name;

					if (item.type === ITEM_ATTACK || item.type === ITEM_MAGIC_ATTACK)
						msg += ' (' + item.attack + 'atk)';
					else if (item.type === ITEM_DEFEND)
						msg += ' (' + item.defense + 'def)';
					else if (item.type === ITEM_LUCK)
						msg += ' (' + item.luck + 'lck)';
				}

				say(msg);
			} else {
				say('what? idk try \x02.littleguy help\x02');
			}
		}

		else if (tag === '.adventure' || tag === '.adv') {
			if (!User.exists(userID)) {
				say(Utils.pronoun() + ' u dont even have a little guy');
				return;
			}

			var user = new User(userID);

			if (ADV_COOLDOWN.hasOwnProperty(userID)) {
				let seconds = ADV_COOLDOWN[userID] - Utils.time();

				if (seconds > 0) {
					say(Utils.pronoun() + ' u just went on an adventure, wait ' + seconds + ' seconds');
					return;
				} else {
					delete ADV_COOLDOWN[userID];
				}
			}

			var equippedName = user.equipped,
				equippedItem = Items[equippedName];

			if (equippedName === undefined || Items.hasOwnProperty(equippedName) !== true)
				equippedItem = undefined;

			var seed1 = Utils.rand(0, 20), // luck
				seed2 = Utils.rand(1, 5) * 10, // normal money
				seed3 = Utils.rand(10, 14) * 10, // treasure money
				seedxp1 = Utils.rand(1, 4), // shit xp
				seedxp2 = Utils.rand(6, 10); // normal xp

			if (seed1 === 0) {
				user.gold += seed3;
				user.xp += seedxp1;
				ADV_COOLDOWN[userID] = Utils.time() + ADV_TIMEOUT;
				say('ur little guy ' + user.sprite + ' found a treasure! u get ' + seed3 + ' little gold coins');
				return;
			} else if (seed1 === 1) {
				say('ur little guy ' + user.sprite + ' found nothing to do');
				return;
			} else if (seed1 === 2) {
				let accident = Utils.randItem(Accidents),
					seed = Utils.rand(0, 100);

				user.xp += seedxp1;

				// chance to get saved: [0, 100] + equipped > 80
				if (equippedItem && equippedItem.type === ITEM_DEFEND) {
					if (seed + equippedItem.defense > 80) {
						ADV_COOLDOWN[userID] = Utils.time() + ADV_TIMEOUT;
						say('ur ' + user.sprite + ' little guy ' + accident + ' but thanks to ur ' + equippedName + ' its fine');
						return;
					}
				} else if (equippedItem && equippedItem.type === ITEM_LUCK) {
					if (seed + equippedItem.luck > 80) {
						ADV_COOLDOWN[userID] = Utils.time() + ADV_TIMEOUT;
						say('ur ' + user.sprite + ' little guy ' + accident + ' but thanks to ur lucky ' + equippedName + ' its fine');
						return;
					}
				}

				if (seed < 4) {
					ADV_COOLDOWN[userID] = Utils.time() + ADV_TIMEOUT;
					say('ur ' + user.sprite + ' little guy ' + accident + ' but its ok, everything is going to be fine~');
				} else {
					ADV_COOLDOWN[userID] = Utils.time() + ACCIDENT_TIMEOUT;
					say('ur ' + user.sprite + ' little guy ' + accident + '. +8 mins to cooldown.');
				}

				return;
			} else if (seed1 === 3) {
				let method = Utils.rand(0, 10) < 4 ? 'steal from' : 'kill',
					monster = Utils.randItem(Monsters);

				user.xp += seedxp1;

				ADV_COOLDOWN[userID] = Utils.time() + ADV_TIMEOUT;
				say('ur little guy ' + user.sprite + ' tried to ' + method + ' ' + monster + ' but failed');
				return;
			} else {
				let method = Utils.rand(0, 10) < 4 ? 'steals from' : 'kills',
					monster = Utils.randItem(Monsters);

				user.gold += seed2;
				user.xp += seedxp2;
				ADV_COOLDOWN[userID] = Utils.time() + ADV_TIMEOUT;
				say('ur little guy ' + user.sprite + ' ' + method + ' ' + monster + '! +' + seed2 + ' gold coins +' + seedxp2 + ' xp');
				return;
			}
		}

		else if (tag === '.raid' || tag === '.dungeon' || tag === '.partyraid') {
			var blacklist = Settings.get('blacklist');

			if (blacklist && blacklist.hasOwnProperty(event.to) === true) {
				whisper('this chan has been blacklisted: ' + blacklist[event.to].reason);
				return;
			}

			if (!User.exists(userID)) {
				say(Utils.pronoun() + ' u cant go to a raid without a little guy, try \x02.class\x02');
				return;
			}

			if (event.public !== true) {
				say('try to make a raid in a channel ' + Utils.pronoun());
				return;
			}

			var user = new User(userID);

			if (ADV_COOLDOWN.hasOwnProperty(userID)) {
				let seconds = ADV_COOLDOWN[userID] - Utils.time();

				if (seconds > 0) {
					say(Utils.pronoun() + ' u just went on an adventure, wait ' + seconds + ' seconds');
					return;
				} else {
					delete ADV_COOLDOWN[userID];
				}
			}

			var channel = event.to;

			if (RAIDS.hasOwnProperty(channel)) {
				let raid = RAIDS[channel];

				if (raid.locked === true) {
					say(Utils.pronoun() + ' this chans raid already started');
				} else if (raid.users.hasOwnProperty(userID)) {
					say(Utils.pronoun() + ' ur little guy is already in this raid');
				} else {
					raid.join(userID, user);
					say('added your little guy to the queue');
				}
			} else {
				var raid = new Raid(channel),
					creation = Utils.time(),
					interval;

				RAIDS[channel] = raid;
				raid.join(userID, user);

				say('u create a queue for ' + channel);

				function loop() {
					var now = Utils.time();

					if (now - creation <= RAID_TIMEOUT) {
						speak('waiting for more players...');
						return;
					}

					raid.locked = true;
					clearInterval(interval);

					var littleGuyList = [],
						playerCount = 0;

					for (let userID in raid.users) {
						let user = raid.users[userID];

						littleGuyList.push(user.sprite + ' ' + user.nick);
						playerCount++;
					}

					littleGuyList = littleGuyList.join(', ');

					speak('\x02' + channel + '\'s ' + raid.dungeon.name + '\x02 is starting! little guys: ' + littleGuyList);

					var steps = {},
						goldPool = 0,
						xpPool = 0,
						killPool = 0,
						status;

					try {
						steps = raid.realize();
					} catch(ex) {
						speak('sorry, something went wrong with the raid. pls do not try again.');
						console.log(ex);
						return;
					}

					for (let i = 0; i < steps.length; i++) {
						let step = steps[i],
							messages = [];

						var user = step.user;

						switch(step.type) {
						case RAID_STEP_LORE_INTRO:
							messages.push.apply(messages, raid.dungeon.lore_intro);
							break;

						case RAID_STEP_KILL:
							let { monsterCount, monsterName, gold, xp } = step;

							messages.push(user.sprite + ' ' + user.nick + '\'s ' + user.class + ' kills ' + monsterCount + ' ' + monsterName);

							goldPool += gold;
							xpPool += xp;
							killPool += monsterCount;
							break;

						case RAID_STEP_BOSS_INTRO:
							messages.push.apply(messages, raid.dungeon.lore_boss);
							break;

						case RAID_STEP_ATTACK_BOSS:
							let { attack, part } = step;

							let damages = {
								bad: [
									'no damage',
									'almost no damage',
									'zero damage'
								],
								ok: [
									'some damage',
									'bleeding damage',
									'piercing damage',
									'puncture damage',
									'bludgeoning damage'
								],
								nice: [
									'a shit ton of damage',
									'a bunch of damage'
								]
							};

							let typeOfDamage = 'ok';
							if (attack < 0)
								typeOfDamage = 'bad';
							else if (attack > 0)
								typeOfDamage = 'nice';

							messages.push(user.sprite + ' ' + user.nick + '\'s ' + user.class + ' deals ' + Utils.randItem(damages[typeOfDamage]) + ' to ' + raid.dungeon.boss.name + '\'s ' + part);
							break;

						case RAID_STEP_FAIL:
							messages.push.apply(messages, raid.dungeon.lore_fail);
							messages.push('not enough damage was done to defeat ' + raid.dungeon.boss.name + '. you leave behind ' + goldPool + ' gold and run to safety.');
							status = 'fail';
							break;

						case RAID_STEP_WIN:
							messages.push.apply(messages, raid.dungeon.lore_win);
							messages.push('you defeat ' + raid.dungeon.boss.name + ' and clear ' + raid.dungeon.name + '.');
							status = 'win';
							break;

						case RAID_LAST_STEP:
							if (status === 'win') {
								xpPool += RAID_BOSS_REWARD_XP;
								let rewardedGold = Math.floor(goldPool / playerCount),
									rewardedXp = Math.floor(xpPool / playerCount);

								for (let userID in raid.users) {
									let user = raid.users[userID];

									user.gold += rewardedGold;
									user.xp += rewardedXp;
									ADV_COOLDOWN[userID] = Utils.time() + RAID_ADVENTURE_TIMEOUT;
								}

								// messages.push('\x02> RAID STATUS: SUCCESS!\x02');
								messages.push('\x033                \x038 gold:\x03 ' + goldPool + ' (' + rewardedGold + ' each)');
								messages.push('\x033 \\   /  o  __   \x039 xp:\x03 ' + xpPool + ' (' + rewardedXp + ' each)');
								messages.push('\x033  \\^/   |  | |  \x0313 monsters:\x03 ' + killPool);
								// messages.push('ok congrats go away now');
							} else {
								xpPool = Math.floor(xpPool / 4);
								let rewardedXp = Math.floor(xpPool / playerCount);

								for (let userID in raid.users) {
									let user = raid.users[userID];

									user.xp += rewardedXp;
									ADV_COOLDOWN[userID] = Utils.time() + ACCIDENT_TIMEOUT;
								}

								// messages.push('\x02> RAID STATUS: FAIL!\x02');
								messages.push('\x034   _           \x038 gold:\x03 0 (' + goldPool + ' dropped)');
								messages.push('\x034 _|_  _  o  |  \x039 xp:\x03 ' + xpPool + ' (' + rewardedXp + ' each)');
								messages.push('\x034  |  (_| |  |  \x0313 monsters:\x03 ' + killPool);
							}

							setTimeout(() => {
								raid.destroy();
								delete RAIDS[channel];
							}, RAID_MESSAGE_INTERVAL * Math.max(steps.length - 2, 10));
						}

						setTimeout(() => {
							messages.forEach(msg => speak(msg));
						}, RAID_MESSAGE_INTERVAL * i);
					}
				}

				interval = setInterval(loop, 20000);
			}
		}

		else if (tag === '.shop') {
			if (!User.exists(userID)) {
				say(Utils.pronoun() + ' u dont even have a little guy');
				return;
			}

			var user = new User(userID),
				shop = new Shop(user);

			if (args.length === 0) {
				var available = shop.availableItems(),
					msg = Utils.randItem(Messages.shop_welcome);

				if (available.length === 0)
					msg += '. there are no items right now, check again later';
				else
					msg += ' (buy with \x02.shop <name>\x02): ';

				var items = [];

				for (let i = 0; i < available.length; i++) {
					let itemName = available[i],
						item = Items[itemName],
						temp = [];

					temp.push(item.name);

					if (item.hasOwnProperty('ranking') && ItemRanking.hasOwnProperty(item.ranking))
						Object.assign(item, ItemRanking[item.ranking]);

					if (item.sprite)
						temp.push(item.sprite);

					if (item.type === ITEM_ATTACK || item.type === ITEM_MAGIC_ATTACK)
						temp.push('(' + item.attack + 'atk)');
					else if (item.type === ITEM_DEFEND)
						temp.push('(' + item.defense + 'def)');
					else if (item.type === ITEM_LUCK)
						temp.push('(' + item.luck + 'lck)');

					temp.push(item.price + ' gold');

					items.push(temp.join(' '));
				}

				msg += items.join(' | ');

				say(msg);

			} else if (args.length >= 1) {
				var desiredItemName = args.join(' ').trim();

				if (Items.hasOwnProperty(desiredItemName) !== true) {
					say(Utils.pronoun() + ' \x02' + desiredItemName + '\x02 isnt a thing');
					return;
				}

				if (user.items.includes(desiredItemName)) {
					say('u already have ' + desiredItemName);
					return;
				}

				if (shop.isAvailable(desiredItemName) !== true) {
					say(Utils.pronoun() + ' the old man doesnt have a ' + desiredItemName);
					return;
				}

				var item = Items[desiredItemName];

				if (item.hasOwnProperty('ranking') && ItemRanking.hasOwnProperty(item.ranking))
					Object.assign(item, ItemRanking[item.ranking]);

				if (user.gold < item.price) {
					say('u dont have enough little gold coins for a ' + desiredItemName);
					return;
				}

				user.gold -= item.price;
				user.items.push(desiredItemName);
				user.items = user.items; // ????????????

				if (item.sprite)
					say('you buy ' + item.sprite + ' \x02' + item.name + '\x02 for ' + item.price + ' gold coins. equip it with \x02.lg e ' + item.name + '\x02');
				else
					say('you buy \x02' + item.name + '\x02 for ' + item.price + ' gold coins. equip it with \x02.lg e ' + item.name + '\x02');
			}
		}
	});

	return;
}

module.exports = {
	setup,
	handler
};
