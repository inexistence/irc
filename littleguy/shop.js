const debug = require('debug')('shop');

const Items = require('./objects/items.json');
const ItemRanking = require('./objects/item-ranking.json');
const Classes = require('./objects/classes.json');

const ITEM_ATTACK = 0;
const ITEM_MAGIC_ATTACK = 1;
const ITEM_DEFEND = 10;
const ITEM_LUCK = 11;

const CLASS_TYPE_NORMAL = 0;
const CLASS_TYPE_MAGIC = 1;

/*
 * formula used for the prices
 * function y(x){ return 400 * Math.pow((2*x+1), 2) }
 * does not apply on the celestial and adamantite items
 *
 * regexp
 * ^(*.)$
 * "\1": {\n\t"name": "\1",\n\t"type": 1,\n\t"attack": 1,\n\t"sprite": "",\n\n\t"price": 0,\n\t"xpMin": 1,\n\t"xpMax": 2\n},
 */

class Shop {
	constructor(user) {
		this.user = user;
	}

	availableItems() {
		var xp = this.user.xp,
			inv = this.user.inv,
			Class = Classes[this.user.class],
			available = [];

		for (let itemName in Items) {
			if (Items.hasOwnProperty(itemName) !== true)
				continue;

			if (inv.items.includes(itemName))
				continue;

			let item = Items[itemName];

			if (item.hasOwnProperty('ranking') && ItemRanking.hasOwnProperty(item.ranking))
				Object.assign(item, ItemRanking[item.ranking]);

			if (item.type === ITEM_ATTACK && Class.type !== CLASS_TYPE_NORMAL)
				continue;

			if (item.type === ITEM_MAGIC_ATTACK && Class.type !== CLASS_TYPE_MAGIC)
				continue;

			if (item.xpMin <= xp && xp <= item.xpMax)
				available.push(itemName);
		}

		return available;
	}

	// idfk
	isAvailable(itemName) {
		var item = Items[itemName],
			inv = this.user.inv,
			xp = this.user.xp,
			Class = Classes[this.user.class]

		if (Items.hasOwnProperty(itemName) !== true)
			return false;

		if (inv.items.includes(itemName))
			return false;

		if (item.hasOwnProperty('ranking')) {
			let desiredRank = item.ranking;

			if (ItemRanking.hasOwnProperty(desiredRank))
				Object.assign(item, ItemRanking[desiredRank]);
			else
				debug('the item ' + item.name + ' cant have the ranking ' + desiredRank);
		}

		if (item.type === ITEM_ATTACK && Class.type !== CLASS_TYPE_NORMAL)
			return false;

		if (item.type === ITEM_MAGIC_ATTACK && Class.type !== CLASS_TYPE_MAGIC)
			return false;

		if (item.xpMin <= xp && xp <= item.xpMax)
			return true;

		return false;
	}
}

module.exports = Shop;