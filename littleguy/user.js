const debug = require('debug')('User');

const Storage = require('./storage');
const Utils = require('./utils');
const Littleguys = new Storage(__dirname + '/littleguys');
const Inventories = new Storage(__dirname + '/inventory');
setInterval(() => Littleguys.consolidate(), 60000);
setInterval(() => Inventories.consolidate(), 60000);
Littleguys.load(() => debug('littleguys loaded'));
Inventories.load(() => debug('inventories loaded'));

const Classes = require('./objects/classes.json');
const Items = require('./objects/items.json');

const DEFAULT_USER = {
	nick: undefined,
	class: undefined,
	gold: 0,
	xp: 0
};

const DEFAULT_INV = {
	equipped: undefined,
	items: []
};

class User {
	static create(userID, document) {
		debug('create', userID);
		var user = Object.assign({}, DEFAULT_USER, document);

		Inventories.put(userID, DEFAULT_INV);
		Littleguys.put(userID, user);
		return new User(userID);
	}

	static exists(userID) {
		debug('exists', userID);
		var user = Littleguys.exists(userID),
			inv = Inventories.exists(userID);

		return user && inv;
	}

	static ranking() {
		var sortable = [];

		for(let userID in Littleguys.MEMORY) {
			if (Littleguys.MEMORY.hasOwnProperty(userID) !== true)
				continue;

			var user = Littleguys.MEMORY[userID];

			sortable.push([
				user.nick,
				user.gold + user.xp
			]);
		}

		return sortable.sort((a, b) => {
			return b[1] - a[1]
		}).slice(0, 10);
	}

	constructor(userID) {
		debug('constructor', userID);
		this.userID = userID;
		this.doc = Littleguys.get(userID);
		this.inv = Inventories.get(userID);

		if (this.doc === undefined)
			throw new Error('u didnt check if the document existed haha xd');

		if (this.inv === undefined) {
			Inventories.put(userID, DEFAULT_INV);
			this.inv = Object.assign({}, DEFAULT_INV);
		}
	}

	set xp(value) {
		this.doc.xp = value;
		Littleguys.put(this.userID, this.doc);
	}

	get xp() {
		return this.doc.xp;
	}

	set class(desired) {
		debug('set class', this.userID);
		this.doc.class = desired;
		Littleguys.put(this.userID, this.doc);
	}

	get class() {
		debug('get class', this.userID);
		return this.doc.class;
	}

	set gold(amount) {
		this.doc.gold = amount;
		Littleguys.put(this.userID, this.doc);
	}

	get gold() {
		return this.doc.gold;
	}

	set nick(nick){
		throw new Error('u cant change this dummy');
	}

	get nick() {
		return this.doc.nick;
	}

	set equipped(itemname) {
		debug('set equipped', itemname);
		this.inv.equipped = itemname;
		Inventories.put(this.userID, this.inv);
	}

	get equipped() {
		debug('get equipped');
		return this.inv.equipped;
	}

	set items(items) {
		this.inv.items = items;
		Inventories.put(this.userID, this.inv);
	}

	get items() {
		return this.inv.items;
	}

	set sprite(sprite) {
		throw new Error('srsly?');
	}

	get sprite() {
		var Class = Classes[this.doc.class],
			equippedName = this.inv.equipped,
			equippedItem = Items[equippedName];

		if (equippedName === undefined || Items.hasOwnProperty(equippedName) !== true)
			equippedItem = undefined;

		if (equippedItem)
			return Class.sprite + equippedItem.sprite;
		else
			return Class.sprite;
	}
}

module.exports = User;