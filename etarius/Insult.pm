#!/usr/bin/env perl
# vim: sw=8 sts=8 noet
package Insult;
use strict;
use warnings;
no warnings 'uninitialized';

use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw(getinsult);

my (%data, @sentences);
sub getdata {
	$data{$_[0]}->[rand @{$data{$_[0]}}]
}
sub load_data {
	open my $fh, '<', $_[0] or die "can't load data from $_[0]: $!";
	while (<$fh>) {
		chomp;
		s/^([^\t]*)\t//;
		$data{$1} = [ split /,/ ];
	}
}
sub load_sentences {
	open my $fh, '<', $_[0] or die "can't load sentences from $_[0]: $!";
	chomp(@sentences = <$fh>);
}

my $dirname	= $0 =~ s|[^/]+$||r;
load_data($dirname . 'data');
load_sentences($dirname . 'sentences');
sub getinsult {
	my $sentence = $_[0] // $sentences[rand @sentences];
	while ($sentence =~ /%([^%+]+)(?:\+([1-5]))?%/) {
		no warnings 'uninitialized';
		my $phrase = getdata($1);
		if ($2) {
			$sentence =~ s/%$1\+$2%/$phrase/g;
		} else {
			$sentence =~ s/%$1%/$phrase/g;
		}
	}
	$sentence;
}
1
