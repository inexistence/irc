const Shitposter = require('./FRAMEWORK');

var bot = new Shitposter({
	nick: 'away_message',
	host: 'irc.rizon.net',
	port: 6697,
	tls: true
});

const client = bot.client;

var ready = false;

bot.on('ready', () => {
	client.join('#homescreen');

	setTimeout(() => {
		ready = true;
	}, 200);
});

const messages = {};

client.on('command', event => {
	if (!ready)
		return;

	if (event.command.tag === '.away') {
		if (event.command.args.length === 0 && messages.hasOwnProperty(event.from) === true) {
			delete messages[event.from];
			client.notice(event.from, 'deleted ur message');
			return;
		}

		let msg = event.command.args.join(' ');

		if (msg.length <= 2) {
			client.notice(event.from, 'thats too short');
			return;
		}

		messages[event.from] = msg;
		client.notice(event.from, 'set ur message to "' + msg + '"');

		return;
	}

	else if (event.command.tag === '.rejoin')
		client.join('#homescreen');
});

client.on('message', event => {
	if (!ready || event.public !== true)
		return;

	for (let nick in messages) {
		if (messages.hasOwnProperty(nick) !== true)
			continue;

		console.log([messages[nick], nick, event.message, event.message.startsWith(nick)]);

		if (event.message.startsWith(nick) || event.message.endsWith(nick)) {
			let message = messages[nick];

			client.send(event.to, event.from + ': This is an automatic message sent on behalf of [' + nick + ']: ' + message);
		}
	}
})