const fs = require('fs');
const Shitposter = require('./FRAMEWORK.js');

var bot = new Shitposter({
	nick: 'quote_scrape',
	host: 'irc.rizon.net'
});

var client = bot.client,
	i = 0;

var quotes = [],
	current = 0;

var target = '#homescreen',
	limit = 8725;

bot.on('ready', () => {
	bot.repeat(850, () => {
		if (current === limit)
			return console.log('donezo');

		current++;
		client.send('Taigabot', '.q ' + target + ' ' + current);
	});
});

client.on('message', event => {
	if (event.from !== 'Taigabot')
		return;

	quotes.push(event.message);
});

bot.on('run', () => {
	i++;

	if(i % 20 === 0)
		console.log('we\'ve run ' + i + ' times');
});

process.on('SIGINT', () => {
	console.log('caught interrupt');

	var data = JSON.stringify(quotes, undefined, 2),
		file = fs.writeFileSync('quotes-' + target + '.txt', data);

	console.log('done');
	client.quit();
	process.exit();
});
