const EventEmitter = require('events');
const net = require('net');
const tls = require('tls');

const slate = require('slate-irc');

class Shitposter extends EventEmitter {
	constructor(config) {
		super();

		if(!config.nick) config.nick = 'farmer';
		if(!config.host) config.host = 'irc.rizon.net';
		if(!config.port) config.port = 6667;

		slate.prototype.writeUnsafe = function(str, fn) {
			console.log('CLIENT> ' + str);
			this.stream.write(str + '\r\n', fn);
		};

		var stream;

		if(config.tls)
			stream = tls.connect( { port: config.port, host: config.host, rejectUnauthorized: false });
		else
			stream = net.connect( config.port, config.host );

		var client = slate( stream );

		stream.pipe(process.stdout);

		stream.on('error', (err) => { this.emit('error', err) });
		client.on('welcome', () => { this.emit('ready') });

		client.on('message', evt => {
			var target = evt.to,
				firstChar = target[0];

			if(!target || !firstChar)
				return;

			// https://tools.ietf.org/html/rfc1459#section-1.3
			// https://tools.ietf.org/html/rfc2812#section-1.3
			if(firstChar === '&' || firstChar === '#' || firstChar === '+' || firstChar === '!')
				evt.public = true;
			else
				evt.public = false;
		});

		client.on('message', evt => {
			var msg = evt.message || '',
				args = msg.split(' ');

			var command = args.shift().toLowerCase(),
				firstChar = command[0];

			if(!command || !firstChar)
				return;

			// common bot command triggers
			if(firstChar === '.' || firstChar === ':' || firstChar === '!' || firstChar === '+') {
				evt.command = {
					tag: command,
					args: args,
					target: evt.public ? evt.to : evt.from
				};

				client.emit('command', evt);
			}
		});

		this.nick = config.nick;
		this.stream = stream;
		this.client = client;

		if(config.pass)
			client.pass(config.pass);

		if(config.randomizenick) {
			this.randNick();

			setInterval(() => {
				this.randNick();
			}, config.randomizenick);
		} else
			client.nick(config.nick);

		client.user(config.nick, config.nick);
	}

	repeat(time, func) {
		return setInterval(() => {
			func();
			this.emit('run');
		}, time);
	}

	randNick() {
		var random = '',
			chars = 'ybndrfg8ejkmcpqxot1uwisza345h769', // z-base-32
			length = 12;

		var baseNick = this.nick;

		for(var i = length; i > 0; --i)
			random += chars[Math.floor(Math.random() * chars.length)];

		this.client.nick(baseNick + random);
	}
}

module.exports = exports = Shitposter;