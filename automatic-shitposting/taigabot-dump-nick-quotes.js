const fs = require('fs');
const readline = require('readline');
const Shitposter = require('./FRAMEWORK.js');

const nicksfile = readline.createInterface({
	input: fs.createReadStream('quotes-nicks-#homescreen.txt')
});

var bot = new Shitposter({
	nick: 'quote_dumper',
	host: 'irc.rizon.net'
});

var client = bot.client,
	i = 0;

var nicks = [],
	quotes = {},
	current = 0,
	limit = 2;

var targetNick,
	targetIndex = 0;

nicksfile.on('line', nick => {
	nicks.push(nick.trim())
});

bot.on('ready', () => {
	bot.repeat(900, () => {
		if (current === limit) {
			targetIndex++;
			current = 0;
		}

		targetNick = nicks[targetIndex];

		if (!targetNick)
			return console.log('no more nick');

		current++;
		client.send('Taigabot', '.q ' + targetNick + ' ' + current);
	});
});

client.on('message', event => {
	if (event.from !== 'Taigabot')
		return;

	if (event.message.startsWith('No quotes found'))
		targetIndex++;

	else if (event.message.startsWith('I only have'))
		targetIndex++;

	if (quotes.hasOwnProperty(targetNick) !== true)
		quotes[targetNick] = [];

	quotes[targetNick].push(event.message);

	var regex = /\[\d+\/(\d+)\] \<(.*)/,
		match = event.message.match(regex);
	
	if (match && match[1])
		limit = parseInt(match[1], 10);
});

bot.on('run', () => {
	i++;

	if(i % 20 === 0)
		console.log('we\'ve run ' + i + ' times');
});

process.on('SIGINT', () => {
	console.log('caught interrupt');

	var data = JSON.stringify(quotes, undefined, 2),
		file = fs.writeFileSync('quotes-individual-#homescreen.txt', data);

	console.log('done');
	client.quit();
	process.exit();
});
