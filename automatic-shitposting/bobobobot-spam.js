const Shitposter = require('./FRAMEWORK.js');

var bot = new Shitposter({
	nick: 'spam_bobobobot',
	host: 'irc.rizon.net',
	tls: true,
	port: 6697
});

var client = bot.client,
	i = 0;

var target = 'Taigabot',
	msgTarget = '#homescreen',
	message = '.bots',
	targetIsOnline = false,
	targetIsInChan = false;

bot.on('ready', () => {
	bot.repeat(1000, () => {
		client.write('ISON ' + target);
	});

	bot.repeat(20000, () => {
		client.names(msgTarget);
	});

	if (msgTarget.startsWith('#'))
		client.join(msgTarget);

	client.on('names', event => {
		for(let i = 0; i < event.names.length; i++) {
			if ((event.names[i].name || '').toLowerCase() === target.toLowerCase())
				targetIsInChan = true;
		}
	});

	client.on('data', event => {
		if (event.command !== 'RPL_ISON')
			return;

		if (event.trailing.includes(target))
			targetIsOnline = true;
		else
			targetIsOnline = false;
	});

	client.on('kick', event => {
		if (event.client === target)
			targetIsInChan = false;
	});

	client.on('join', event => {
		if (event.nick === target)
			targetIsInChan = true;
	});

	client.on('part', event => {
		if (event.nick === target)
			targetIsInChan = false;
	});

	client.on('quit', event => {
		if (event.nick === target)
			targetIsInChan = false;
	});

	// spam often
	bot.repeat(400, () => {
		if (!targetIsOnline)
			return;

		if (!targetIsInChan)
			return;

		client.send(msgTarget, message);
	});

	bot.repeat(4 * 1000, () => {
		if (!targetIsOnline)
			return;

		//client.send(msgTarget, '');
		//client.send(msgTarget, '');
	});

});

bot.on('run', () => {
	i++;

	if(i % 20 === 0)
		console.log('we\'ve run ' + i + ' times');

	/*if(i === 100) {
		console.log('done');
		client.quit();
		process.exit();
	}*/
});
