const fs = require('fs');

const Shitposter = require('./FRAMEWORK.js');

const SHITPOSTING = fs.readFileSync('/tmp/ascii-art/colour/longcat', { encoding: 'utf8' }).replace(/\r/g, '').split('\n');

var bot = new Shitposter({
	nick: 'asciidump',
	randomizenick: 60000,
	host: 'irc.krustykrab.restaurant',
	port: 6697,
	tls: true
});

var target = '##testing',
	rate = 100;

var client = bot.client,
	i = 0,
	currentLine = 0;

bot.on('ready', () => {
	client.join(target);

	bot.repeat(rate, () => {
		var line = SHITPOSTING[currentLine++];

		if(typeof line === 'undefined')
			return;

		if(typeof line === 'string' && line.length < 1)
			line = ' ';

		client.send(target, line);
	});
});

bot.on('run', () => {
	i++;

	if(i % 100 === 0)
		console.log('we\'ve run ' + i + ' times');
});
