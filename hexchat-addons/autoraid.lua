hexchat.register('autoraid', 1, 'joins a raid automatically')

local function handle_message(words, eols)
	if words[1] == ':bro!~bro@hey.join.the.raid' then
		local line = table.concat(words, ' ')

		if string.match(line, ': u create a queue for #fortress') then
			hexchat.command('MSG #fortress .raid')
		end
	end

	return hexchat.EAT_NONE
end

hexchat.hook_server('PRIVMSG', handle_message)
