local events = {
	'Channel Message',
	'Channel Action',
	'Channel Msg Hilight',
	'Channel Action Hilight'
}

local function callback(event)
	return function (args)
		if #args < 2 then
			return hexchat.EAT_NONE
		end

		local nick = hexchat.strip(args[1], false, false)
		local msg = args[2]

		if nick == 'Taigabot' or nick == 'paprika' then
			if string.sub(msg, 0, 2) == '\x02\x02' or string.sub(msg, 0, 12) == '[URL] image/' then
				return hexchat.EAT_ALL
			end
		end

		return hexchat.EAT_NONE
	end
end

for _, event in ipairs(events) do
	hexchat.hook_print(event, callback(event), hexchat.PRI_LOWEST)
end

hexchat.register('ignore bot intros', 1, 'ignore taiga/paprika user-defined intros')
print('ignore bot intros loaded')
