-- for when you don't want to see some idler's join/quit network-wide but
-- you don't want to /ignore their messages

local ignored_nicks = {
	"zacwalls",
	"Karelia",
	"longname"
}

----------

local events = {
	'Join',
	'Quit'
}

local function should_ignore(array, item)
	for _, value in pairs(ignored_nicks) do
		if value == item then
			return true
		end
	end

	return false
end

local function callback(args)
	if should_ignore(args[1]) then
		return hexchat.EAT_ALL
	end
end

for _, event in ipairs(events) do
	hexchat.hook_print(event, callback)
end

hexchat.register('hide join/quits', 1, 'hides join/quit messages of specified nicks network-wide')
print('hide join/quits')
