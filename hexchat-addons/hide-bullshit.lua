--[[
	-------------
	HIDE BULLSHIT
	-------------
	hexchat script to filter out specific messages based on their content
	by inexistence!
	
	it can accept either exact messages or lua patterns[1].
	it can also take multiple nicks per "set of ignored messages", because
	some users spread their bullshit across multiple nicks.
	
	[1]: http://lua-users.org/wiki/PatternsTutorial
]]--

-- "exact": received message must be equal to text
-- "contains somewhere": received message must contain text somewhere
local ignored = {
	{
		nicks = {"IRSSucks", "IRSSucks_"},
		mode = "exact",
		messages = {
			"based!",
			"based",
			"BASED",
			"CRINGE"
		}
	},
	{
		nicks = {"byyzbot", "byyzbot_"},
		mode = "contains somewhere",
		messages = {
			"I hardly know her!",
			"https://i.imgur.com/5ADdY5U.jpg",
			"AHHHHHHHHHHHHHHHH YESSSSSS",
			"Bloomberg 0",
			"fuck you dont ping me",
			"IS A MASSIVE RETARD",
			"POLTARD ALERT"
		}
	},
	{
		nicks = {"nevodka"},
		mode = "exact",
		messages = {
			"bloom" -- fucking obsessive
		}
	}
}

---------------------

local events = {
	'Channel Message',
	'Channel Action',
	'Channel Msg Hilight',
	'Channel Action Hilight'
}

local function contains(array, item)
	for key, value in pairs(array) do
		if value == item then
			return true
		end
	end

	return false
end

local function matches_any(array, item)
	for key, value in pairs(array) do
		if string.match(item, value) then
			return true
		end
	end

	return false
end

local function callback(event)
	return function (args)
		if #args < 2 then
			return hexchat.EAT_NONE
		end

		local nick = hexchat.strip(args[1], false, false)
		local msg = args[2]

		-- look at this sad pyramid
		-- i blame the lack of `continue`
		for _, ignorable in pairs(ignored) do
			for _, ignored_nick in pairs(ignorable["nicks"]) do
				if nick == ignored_nick then
					local mode = ignorable["mode"]
					local msgs = ignorable["messages"]
					
					if mode == "exact" then
						if contains(msgs, msg) then
							return hexchat.EAT_ALL
						end
					elseif mode == "contains somewhere" then
						if matches_any(msgs, msg) then
							return hexchat.EAT_ALL
						end
					end
				end
			end
		end

		return hexchat.EAT_NONE
	end
end

for _, event in ipairs(events) do
	hexchat.hook_print(event, callback(event), hexchat.PRI_LOWEST)
end

hexchat.register('hide bullshit', 1, 'script to filter out bullshit')
print('loaded hide bullshit')
