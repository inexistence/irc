hexchat.register('massvoice', 1, 'gives voice to an entire chan')

local function shuffle(tbl)
	local size = #tbl
	for i = size, 1, -1 do
		local rand = math.random(size)
		tbl[i], tbl[rand] = tbl[rand], tbl[i]
	end
	return tbl
end

local function getnicks()
	local nicks = {}
	local ownnick = hexchat.get_info('nick')

	for user in hexchat.iterate('users') do
		if hexchat.nickcmp(user.nick, ownnick) ~= 0 then
			table.insert(nicks, user.nick)
		end
	end

	shuffle(nicks)
	table.insert(nicks, ownnick)
	return nicks
end

local function massmode(mode)
	return function ()
		hexchat.send_modes(getnicks(), mode)
		return hexchat.EAT_HEXCHAT
	end
end

hexchat.hook_command('massvoice', massmode('+v'))
hexchat.hook_command('massdevoice', massmode('-v'))

hexchat.hook_command('masshop', massmode('+h'))
hexchat.hook_command('massdehop', massmode('-h'))

hexchat.hook_command('massop', massmode('+o'))
hexchat.hook_command('massdeop', massmode('-o'))

hexchat.hook_command('massprotect', massmode('+a'))
hexchat.hook_command('massdeprotect', massmode('-a'))

hexchat.hook_command('massowner', massmode('+q'))
hexchat.hook_command('massdeowner', massmode('-q'))

print('mass voice loaded')
