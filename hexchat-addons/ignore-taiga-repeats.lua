local events = {
	'Channel Message',
	'Channel Action',
	'Channel Msg Hilight',
	'Channel Action Hilight'
}

local oldMessage = ''

local function callback(event)
	return function (args)
		if #args < 2 then
			return hexchat.EAT_NONE
		end

		local nick = hexchat.strip(args[1], false, false)
		local msg = args[2]

		if string.len(msg) < 4 then
			return hexchat.EAT_NONE
		end

		if nick == 'Taigabot' then --or nick == 'paprika' then
			if msg == oldMessage then
				return hexchat.EAT_ALL
			end

			oldMessage = msg
		end

		return hexchat.EAT_NONE
	end
end

for _, event in ipairs(events) do
	hexchat.hook_print(event, callback(event), hexchat.PRI_LOWEST)
end

hexchat.register('ignore taigabot repeats', 1, 'ignore taigabot repeating itself')
print('ignore taigabot repeats loaded')
