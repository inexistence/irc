hexchat.register('masshl', 1, 'what tf do you think it does')

local excludes = {
	-- rizon humans
	'juanpunch',
	'adrift',
	'afloat',
	'Acbn',
	'Tabal',
	'Foom',
	'Foohum',
	'uncleleech',
	'ine',
	'FrankTheMagicHobo',
	'ShitladyOfTheSkies',
	'SamAxemas',
	'greenbagels',
	'FrankTheHobo',
	'Underdose',
	'Rance-',
	'gchcetiH',
	'TabalPhone',
	'alan',

	-- rizon bots
	'Taigabot',
	'ChanStat',
	'brobot',
	'bro',
	'ImageIRC',
	'shitbot',
	'Lurija',
	'neo8ball',
	'Short_Circuit',
	'taylorswift',
	'Akikawa',
	'CaptMoose',
	'PAYNUS',
	'mobot',
	'Etarius',
	'kekbot',
	'iko',
	'UNOBot',
	'NeoMoose',
	'e-Sim',
	'CloverOS',
	'CloverOS_AU',
	'CloverOS_FR',
	'tg',
	'm8',

	-- rizon ops
	'Chris',
	'mink',
	'netsec',

	-- rizon services
	'Quotes',
	'Internets',
	'Trivia',
	'Rizon',
	'oink',

	-- jollo humans
	'nill',
	'loli',

	'end i guess'
}

local function has_value(table, val)
	for index, value in ipairs(table) do
		if value == val then
			return true
		end
	end

	return false
end

function shuffle(tbl)
	local size = #tbl
	for i = size, 1, -1 do
		local rand = math.random(size)
		tbl[i], tbl[rand] = tbl[rand], tbl[i]
	end
	return tbl
end

local function command_handler(words, word_eol)
	local nicks = {}
	for user in hexchat.iterate('users') do
		if not string.match(user.nick, 'bot') and not has_value(excludes, user.nick) then
			table.insert(nicks, user.nick)
		end
	end

	shuffle(nicks)

	local spam = table.concat(nicks, ' ')
	hexchat.command('say ' .. spam)
	return hexchat.EAT_HEXCHAT
end

local function fucky_command_handler(words, word_eol)
	local nicks = {}
	for user in hexchat.iterate('users') do
		if not string.match(user.nick, 'bot') then
			table.insert(nicks, user.nick)
		end
	end

	shuffle(nicks)

	local spam = table.concat(nicks, ' ')
	hexchat.command('say ' .. spam)
	return hexchat.EAT_HEXCHAT
end

hexchat.hook_command('masshl', command_handler)
hexchat.hook_command('massmasshl', fucky_command_handler)
print('masshl loaded')
