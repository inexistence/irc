hexchat.register("rainbow", 1, "lets u talk w/ rainbow colors")

hexchat.hook_command("rainbow", function(w, we)
	local s = ""
	for i = 1, #we[2] do
		s = s .. ("\3%02d"):format(({4,7,8,9,3,10,11,12,2,6,13})[i % 11 + 1]) .. we[2]:sub(i,i)
	end
	hexchat.command("say ".. s)
	return hexchat.EAT_HEXCHAT
end)
