local events = {
	'Channel Message',
	'Channel Action',
	'Channel Msg Hilight',
	'Channel Action Hilight'
}

local replaces = {
	{' fur ', ' for '},
	{'fur ', 'for '},
	{'apline', 'alpine'},
	--{'teh ', 'the '},
	{'inexistance', 'inexistence'},
	--{'based ', 'stupid '},
	{'alot ', 'a lot '},
	{' alot', ' a lot'}
}

local edited = false

local function callback(event)
	return function (args)
		if edited or #args < 2 then
			return hexchat.EAT_NONE
		end

		nick = args[1]
		text = args[2]

		local modified = false

		for _, rule in ipairs(replaces) do
			if string.match(text, rule[1]) then
				text = string.gsub(text, rule[1], rule[2]) .. ' and i am a faggot'
				modified = true
			end
		end

		if modified == false then
			return hexchat.EAT_NONE
		end

		edited = true
		hexchat.emit_print(event, nick, text, args[3], args[4], args[5])
		edited = false

		return hexchat.EAT_ALL
	end
end

for _, event in ipairs(events) do
	hexchat.hook_print(event, callback(event))
end

hexchat.register('fix idiots', 1, 'it replaces words for other users, mostly a fix for Acbn saying "fur" instead of "for", since hes a fucking furry')
