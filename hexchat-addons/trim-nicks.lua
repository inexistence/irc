local events = {
	'Channel Message',
	'Channel Action',
	'Channel Msg Hilight',
	'Channel Action Hilight'
}

local edited = false
local limit = 15

local function callback(event)
	return function (args)
		if edited or #args < 2 then
			return hexchat.EAT_NONE
		end

		local nick = args[1]

		-- user limit + user mode(?) + color characters
		local realLimit = limit + (string.len(nick) - string.len(hexchat.strip(nick, false, false)))

		local modified = false

		if string.len(nick) >= realLimit + 1 then
			nick = string.sub(nick, 0, realLimit) .. '+'
			modified = true
		end

		if modified == false then
			return hexchat.EAT_NONE
		end

		edited = true
		hexchat.emit_print(event, nick, args[2], args[3], args[4], args[5])
		edited = false

		return hexchat.EAT_ALL
	end
end

for _, event in ipairs(events) do
	hexchat.hook_print(event, callback(event))
end

hexchat.register('trim nicks', 1, 'trims long nicks')
print('trim nicks loaded')
