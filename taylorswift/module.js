"use strict";

var network,
	settings,
	core,
	modules;

const Storage = require('./storage');

const db = new Storage(__dirname + '/database');

const BENE_COOLDOWN = {};
const JAIL_COOLDOWN = {};
const TDIP_COOLDOWN = {};

const BENE_TIMEOUT = 2 * 60; // seconds
const JAIL_TIMEOUT = 5 * 60;
const TDIP_TIMEOUT = 60 * 60;

function rand(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
}

function time() {
	return Math.floor(Date.now() / 1000);
}

setInterval(() => {
	db.consolidate();
}, 60000);

function setup(bot) {
	({ network, settings, core, modules } = bot);

	if (!core.help)
		throw new Error('requires help core module');

	core.help.add(
		'bene',
		['.bene'],
		'get paid'
	);

	core.help.add(
		'mug',
		['.mug', '.rob'],
		'try to steal money from someone. format: \x02.mug nick\x02'
	);

	core.help.add(
		'bet',
		['.pokies', '.bet'],
		'bet some benebux at the casino format: \x02.bet amount\x02'
	);

	core.help.add(
		'durry',
		['.durry'],
		'buy a "cig"'
	);

	core.help.add(
		'give',
		['.give'],
		'give a lad some help. format: \x02.give nick amount\x02'
	);

	core.help.add(
		'bank',
		['.money', '.wallet', '.balance', '.bank', '.benebux'],
		'check how m uch money u have'
	);

	core.help.add(
		'jailstatus',
		['.jailstatus'],
		'you should know if youre in jail or not man, but u can check it anyway'
	);

	core.help.add(
		'odds',
		['.odds'],
		'list of current rewards and probabilities'
	);

	core.help.add(
		'lottery',
		['.tripledip', '.jackpot', '.lottery'],
		'try to win the lottery'
	);

	db.load(() => console.log('database has been loaded'));

	return;
}

function handler(irc) {
	irc.on('command', event => {
		if (event.shouldIgnore)
			return;

		var { tag, args, admin, target } = event.command,
			nick = ('' + event.from).toLowerCase();

		function handleError(err) {
			irc.notice(nick, 'sorry, an error or something happened');
			console.log(err);
		}

		if(tag === '.bene') {
			var doc = db.get(nick);

			if(doc === undefined) {
				db.put(nick, { money: 500 });
				irc.send(target, 'winz just gave u \x033$500\x03. u now have \x033$500\x03 in ur shiny new account');
				BENE_COOLDOWN[nick] = time() + BENE_TIMEOUT;

			} else {
				if(BENE_COOLDOWN.hasOwnProperty(nick)) {
					let seconds = BENE_COOLDOWN[nick] - time();

					if(seconds > 0) {
						irc.send(target, 'bro ur next payment is in ' + seconds + ' seconds');
						return;
					} else {
						delete BENE_COOLDOWN[nick];
					}
				}

				doc.money += 500;
				db.put(nick, doc);
				irc.send(target, 'winz just gave u \x033$500\x03. u now have \x033$' + doc.money + '\x03');
				BENE_COOLDOWN[nick] = time() + BENE_TIMEOUT;
			}
		}

		else if(tag === '.pokies' || tag === '.bet') {
			if(args.length < 1) {
				irc.send(target, 'mate try .bet <amount>');
				return;
			}

			var amount = args[0];
			amount = parseInt(amount, 10);
			if(!amount || !Number.isInteger(amount)) {
				irc.send(target, 'hey that doesnt look like a valid amount');
				return;
			}

			if(amount <= 1) {
				irc.send(target, 'dont be a cheap cunt');
				return;
			}

			var doc = db.get(nick);

			if(doc === undefined) {
				irc.send(target, 'u dont even have an account');
				return;
			}

			if(doc.money < amount) {
				irc.send(target, 'u dont have enuf money bro');
				return;
			}

			var luck = rand(0, 10);
			if(luck <= 4) {
				doc.money += amount;
				db.put(nick, doc);
				irc.send(target, 'bro you won! wow \x033$' + amount + '\x03, thats heaps g! drinks on u ay');
			} else {
				doc.money -= amount;
				db.put(nick, doc);
				irc.send(target, 'shit man, u lost \x033$' + amount + '\x03. better not let the middy know');
			}
		}

		else if(tag === '.durry') {
			var doc = db.get(nick);

			if(doc === undefined) {
				irc.send(target, 'u dont even have an account');
				return;
			}

			if(doc.money < 5) {
				irc.send(target, 'u gotta put coins in the machine mate');
				return;
			}

			doc.money -= 5;
			db.put(nick, doc);

			irc.send(target, '             (');
			irc.send(target, ' _ ___________ )    u get a "cig" for $5');
			irc.send(target, '[_[___________#');
		}

		else if(tag === '.give') {
			if(args.length < 2) {
				irc.send(target, 'try .give <nick> <amount>');
				return;
			}

			var receiver = args[0].toLowerCase(),
				quantity = args[1];

			quantity = quantity.replace(/[\.\,\$]/gi, '');
			quantity = parseInt(quantity, 10);

			if(!quantity || !Number.isInteger(quantity)) {
				irc.send(target, 'hey that doesnt look like a valid amount');
				return;
			}

			if(quantity <= 1 || nick === receiver) {
				irc.send(target, 'cmon man help a brother out');
				return;
			}

			var owndoc = db.get(nick);

			if(owndoc === undefined) {
				irc.send(target, 'u dont even have an account');
				return;
			}

			if(owndoc.money < quantity) {
				irc.send(target, 'u dont have enough money for that');
				return;
			}

			var targetdoc = db.get(receiver);

			if(targetdoc === undefined) {
				irc.send(target, 'sorry bro theyre with kiwibank');
				return;
			}

			owndoc.money -= quantity;
			db.put(nick, owndoc);

			targetdoc.money += quantity;
			db.put(receiver, targetdoc);

			irc.send(target, 'you gave ' + receiver + ' \x033$' + quantity + '\x03');
		}

		else if(tag === '.mug' || tag === '.rob') {
			if(args.length < 1) {
				irc.send(target, 'try .mug <nick>');
				return;
			}

			if(JAIL_COOLDOWN.hasOwnProperty(nick)) {
				let seconds = JAIL_COOLDOWN[nick] - time();

				if(seconds > 0) {
					irc.send(target, 'ur in jail for another ' + seconds + ' seconds. dont drop the soap!');
					return;
				} else {
					delete JAIL_COOLDOWN[nick];
				}
			}

			var victim = args[0].toLowerCase();

			if(victim === nick) {
				irc.send(target, 'wtf is wrong with you');
				return;
			}

			var owndoc = db.get(nick),
				victimdoc = db.get(victim);

			if(owndoc === undefined) {
				irc.send(target, 'u dont even have an account to put that in');
				return;
			}

			if(victimdoc === undefined) {
				irc.send(target, 'sorry bro theyre with kiwibank');
				return;
			}

			if(victimdoc.money <= 1) {
				irc.send(target, 'they dont have any money to steal');
				return;
			}

			var luck = rand(0, 10),
				stolen = rand(0, victimdoc.money / 2);

			if(luck <= 3) {
				victimdoc.money -= stolen;
				db.put(victim, victimdoc);

				owndoc.money += stolen;
				db.put(nick, owndoc);

				irc.send(target, 'u manage to steal \x033$' + stolen + '\x03 off ' + victim);
			} else {
				irc.send(target, '\x034,4 \x032,2 \x030,1POLICE\x034,4 \x032,2 \x03 Its the police! looks like u got caught. thats five minutes the big house for you!');
				JAIL_COOLDOWN[nick] = time() + JAIL_TIMEOUT;
			}
		}

		else if(tag === '.money' || tag === '.wallet' || tag === '.balance' || tag === '.bank' || tag === '.benebux') {
			var doc = db.get(nick);

			if(doc === undefined) {
				irc.send(target, 'u dont even have an account try .bene');
				return;
			}

			irc.send(target, 'you currently have \x033$' + doc.money + '\x03 in the bnz');
		}

		else if(tag === '.jailstatus') {
			if(JAIL_COOLDOWN.hasOwnProperty(nick)) {
				let seconds = JAIL_COOLDOWN[nick] - time();

				if(seconds > 0) {
					irc.send(target, 'ur in jail for another ' + seconds + ' seconds. dont drop the soap!');
					return;
				} else {
					delete JAIL_COOLDOWN[nick];
				}
			}

			irc.send(target, 'ur not in jail u helmet');
		}

		else if(tag === '.odds') {
			irc.send(target, 'current odds: bene: $500; chance to mug: 30%; bet chance: 40%; jackpot: 1%');
		}

		else if(tag === '.tripledip' || tag === '.jackpot' || tag === '.lottery') {
			var doc = db.get(nick);

			if(doc === undefined) {
				irc.send(target, 'u dont even have an account');
				return;
			}

			if(TDIP_COOLDOWN.hasOwnProperty(nick)) {
				let seconds = TDIP_COOLDOWN[nick] - time();

				if(seconds > 0) {
					irc.send(target, 'pls wait another ' + seconds + ' seconds before trying again');
					return;
				} else {
					delete TDIP_COOLDOWN[nick];
				}
			}

			if(doc.money < 100) {
				irc.send(target, 'tickets are \x033$100\x03. u dont have enough benebux for one');
				return;
			}

			var luck = rand(0, 100),
				won = 100000;

			if(luck <= 1) {
				doc.money += won;
				db.put(nick, doc);
				irc.send(target, '\x038,4!WINNER!\x03 \x038,4!WINNER!\x03 \x038,4!WINNER!\x03 ' + nick + ' just won the jackpot! \x033$' + won + '\x03 has been awarded to them. \x038,4!WINNER!\x03 \x038,4!WINNER!\x03 \x038,4!WINNER!\x03');
			} else {
				doc.money -= 100;
				db.put(nick, doc);
				irc.send(target, 'u bought a ticket for \x033$100\x03. unfortunately u had no luck winning this time');
			}

			TDIP_COOLDOWN[nick] = time() + TDIP_TIMEOUT;
		}
	});

	return;
}

module.exports = {
	setup,
	handler
};
